import unittest
import Circumference_Of_Circle
cir = Circumference_Of_Circle.area()


class Test_circumference(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Executing Class")

    def setUp(self):
        print("Executing test cases")

    def test_findCircumference_positive(self):
        self.assertEqual(cir.findCircumference(3), 19)
        self.assertEqual(cir.findCircumference(1), 7)
        self.assertEqual(cir.findCircumference(10), 63)

    def test_findCircumference_Zero(self):
        self.assertEqual(cir.findCircumference(0), 0)

    def test_findCircumference_negative(self):
        with self.assertRaises(ValueError):
            cir.findCircumference(-5)

    def test_findCircumference_Document(self):
        with patch('requests.get') as mocked_get:
            mocked_get.return_value.ok = True
            mocked_get.return_value.text = 'Success'

            link = "https://www.byju.com/maths/Circumferenceof-a-circle"

            schedule = cir.CircumferenceCalculationDocument(link)
            mocked_get.assert_called_with(link)
            self.assertEqual(schedule, "Documentation is Verified")

            mocked_get.return_value.ok = False

            schedule = cir.CircumferenceCalculationDocument(link)
            mocked_get.assert_called_with(link)
            self.assertEqual(schedule, "Document is Not Verified")

    def tearDown(self):
        print("Stopping the running test cases")

    @classmethod
    def tearDownClass(cls):
        print("Program executed successfully")


if __name__ == '__main__':
    unittest.main()