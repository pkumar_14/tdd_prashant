from Resources import input_file
import requests

class Circumference():
    def findCircumference(self, r):
        PI = 3.142
        return round(2*PI*r)

    def CircumferenceCalculationDocument(self, link):
        response = requests.get(f'{link}')
        if response.ok:
            return "Verified"
        else:
            return "Not Verified"

radius = input_file.myradius
if radius < 0:
    raise ValueError("Radius Can't be Negative")
circleCircumference = Circumference()
Circumference_circle = circleCircumference.findCircumference(radius)
print(Circumference_circle)